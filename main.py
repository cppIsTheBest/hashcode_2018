#!/usr/bin/env

from src.inputParser import *
from src.solver import *
from src.outputGenerator import *
from src.evalSolution import *

# import score calculator from submodule directory
import sys
sys.path.insert(0, './picojr_hashcode18_score')
from score import *

# check correct usage
if len(sys.argv) != 3:
    print("Usage : ./main.py in_file_name out_file_name")
    sys.exit()

# parse input file
const, rides = parse(sys.argv[1])

# compute a solution
orders = solve(const, rides)

# create output file
createOutput(orders, sys.argv[2])

# compute and display score
score = compute_score(sys.argv[1], sys.argv[2])
print("SCORE :")
print("    * raw -> " + str(score.raw_score))
print("    * bonus -> " + str(score.bonus_score))
print("    * total -> " + str(score.raw_score + score.bonus_score))

# compute and display number of orders given
print(str(countOrders(orders)) + " orders where given (maximum is " + str(const[3]) + ").")
print("    -> " + str(score.late) + " arrived late")
#print("    -> The ID of the unassigned rides are : " + str(getUnassignedRidesID(orders, len(rides))))

# print some information
print("Mean ride length is " + str(meanRideLength(rides)))
meanIDLETime, maxIDLETime = infoIDLETimeBetweenTwoRides(orders, rides)
print("Mean idle time between 2 rides is " + str(meanIDLETime))
print("Max idle time between 2 rides is " + str(maxIDLETime))

