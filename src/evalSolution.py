from __future__ import division

def distance(x, y, x2, y2):
    return abs(x - x2) + abs(y - y2)


def countOrders(orders) :
    nbOrders = 0
    for line in orders :
        nbOrders += len(line)

    return nbOrders
    

def meanRideLength(rides):
    mean = 0
    for r in rides:
        (xs, ys, xf, yf, ts, tf, ID) = r
        mean += distance(xs, ys, xf, yf)
    mean /= len(rides)
    return mean


def infoIDLETimeBetweenTwoRides(orders, rides):
    mean = 0
    maximum = 0
    countIDLEPeriods = 0

    for o in orders:
        for i in range(len(o)):
            # get current ride info
            r = rides[o[i]]
            (xs, ys, xf, yf, ts, tf, ID) = r

            # compute wait time between the beginning and the first ride
            if (i == 0):
                waitTime = ts - distance(xs, ys, 0, 0)
                # compute time at which the ride is over and the vehicule free
                t_free = max(distance(xs, ys, 0, 0), ts) + distance(xs, ys, xf, yf)

            # compute wait time between the previous ride and the current ride
            else:
                # get previous ride info
                r_p = rides[o[i-1]]
                (xs_p, ys_p, xf_p, yf_p, ts_p, tf_p, ID_p) = r_p
                waitTime = ts - distance(xs, ys, xf_p, yf_p) - t_free
                # compute time at which the ride is over and the vehicule free
                t_free = max(distance(xs, ys, xf_p, yf_p) + t_free, ts) + distance(xs, ys, xf, yf)

            # if there is a wait time > 0, add it
            if waitTime > 0:
                mean += waitTime

            maximum = max(maximum, waitTime)

        # count possible IDLE times (we take into account the one between simulation start and first ride)
        countIDLEPeriods += len(o)

    mean /= countIDLEPeriods
    return mean, maximum


def getUnassignedRidesID(orders, nbRides):
    assigned = []
    for o in orders:
        for rID in o:
            assigned.append(rID)
    list.sort(assigned)
    unassigned = []
    for i in range(len(assigned)):
        if (i < len(assigned)-1):
            rmax = assigned[i+1]
        else:
            rmax = nbRides
        for rID in range(assigned[i]+1, rmax):
            unassigned.append(rID)

    return unassigned

