#!/usr/bin/env

def parse(file_path):
    """ Parse the input file and create :
        - a list containing the 6 constraints of the problem
        - a list containing the rides
    """
    rides = []

    with open(file_path,"r") as f:
        l = f.readline()
        constraints = l[:-1].split(" ")
        constraints = [int(e) for e in constraints]
        for l in f:
            l = l.split(" ")
            tmp = [int(e) for e in l if e!="\n"]
            rides.append(tmp)

    return constraints, rides


if __name__ == "__main__":
    const,rides = parse("in/a_example.in")
    print("const = " + str(const))
    print("rides = " + str(rides))
