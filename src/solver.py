from evalSolution import *

def sortDeadline(rides):
    """
        Returns the list of rides sorted by priority.
        Each element of the list is itself a list representing a ride, as for
        the input list. But an int is added to each ride to indicate the index
        in the original list (7th element).
        ! the input list is also modified
    """
    sorted_rides = []
    i = 0

    while (i < len(rides)) :
        t_begin_min = 2000000000
        dist_min = 20000
        id_min = -1
        k = 0

        # at each iteration, find the ride with the earliest start deadline 
        for c in rides:
            t_ride = distance(c[0], c[1], c[2], c[3])
            # if earliest start deadline and ride not already sorted
#            if (c[5] - t_ride < t_begin_min and len(c) == 6):
            if (c[4] < t_begin_min and len(c) == 6):
                t_begin_min = c[4]
#               t_begin_min = c[5] - t_ride
                id_min = k
            k += 1

        # add the chosen ride to the sorted list
        rides[id_min].append(id_min);
        sorted_rides += [rides[id_min]]

        i += 1

    return sorted_rides


def initVehiculesInfo(vehiculesNumber):
    """
        Setup the vehicules info objects.
        For each vehicule, the last planned position and matching simulation step
        is stored.
    """
    return [(0, 0, 0) for i in range(0, vehiculesNumber)]
    

def initOrders(vehiculesNumber):
    """
        Setup the object containing all orders given to the vehicules.
        It will be a list in which each element is a list of the indexes of the
        rides assigned to the vehicule (empty if none).
    """
    return [[] for i in range(0, vehiculesNumber)]


def distance(x1,y1,x2,y2):
    """ Return the distance between two coordinates in the grid """
    return abs(x1-x2) + abs(y2-y1)
    

def setOrdre(orders, vehiculeID, orderID):
    """
        Add an order to a vehicule
        - orders is the list storing the rides done by each vehicule
        - vehiculeID is the ID number of the vehicule
        - rideID is the ID number of the ride
    """
    orders[vehiculeID].append(orderID)


def update(vInfo, vehiculeID, ride):
    """
        Update the info on a vehicule, knowing he will do the given ride.
        - vInfo is the vehicules information object
        - vehiculeID is the ID number of the vehicule
        - ride is the ride that the vehicule will do
    """
    [xs, ys, xf, yf, t_begin, t_end, rideID] = ride
    [xv, yv, tv] = vInfo[vehiculeID]
    tf = max(distance(xv, yv, xs, ys) + tv, t_begin) + distance(xs, ys, xf, yf)
    vInfo[vehiculeID] = [xf, yf, tf]


def choice(vInfo, ride, T):
    """
        Given a ride, choose the best vehicule to be assigned to it.
        - vInfo is the vehicules information object
        - ride is the ride to do
        - T is the total number of steps of the simulation
    """
    [xs, ys, xf, yf, t_begin, t_end, rideID] = ride
    vehiculeID = -1
    tfmin = 2000000000

    for i in range(len(vInfo)):
        # if the vehicule is already at the start, we stop the research here
        if (vInfo[i][0] == xs and vInfo[i][1] == ys):
            vehiculeID = i
            break

        # check if the current vehicule can do the ride faster than the
        # previously selected one
        [ok, tf] = checkPossible(vInfo[i], ride)
        if (ok and tf < tfmin):
            vehiculeID = i
            tfmin = tf
    return vehiculeID


def checkPossible(vehiculeInfo, ride):
    """
        Check if it possible to go from a position to another in accordance
        with the deadlines.
        Return the boolean answer and the time at which the vehicule would have
        achieved the ride.
    """
    [xs, ys, xf, yf, t_begin, t_end, rideID] = ride
    [xv, yv, tv] = vehiculeInfo
    tf = max(distance(xv, yv, xs, ys) + tv, t_begin) + distance(xs, ys, xf, yf)
    possible = t_end >= tf

    return possible, tf


def searchHole(orders, rides, r):
    ###########################################################################
    # /!\ This code is really ugly but it allows some improvements on problem c.
    #     I will clean and comment it soon
    ###########################################################################
    (xs, ys, xf, yf, ts, tf, ID) = r
    for vID in range(len(orders)):
        t_free = 0
        for i in range(len(orders[vID])):
            r_c = rides[orders[vID][i]]
            (xs_c, ys_c, xf_c, yf_c, ts_c, tf_c, ID_c) = r_c

            if (i == 0):
                goTo_R = distance(0, 0, xs, ys)
            else:
                r_p = rides[orders[vID][i-1]]
                (xs_p, ys_p, xf_p, yf_p, ts_p, tf_p, ID_p) = r_p
                goTo_R = distance(xf_p, yf_p, xs, ys)

            length_R = distance(xs, ys, xf, yf)
            length_R_c = distance(xs_c, ys_c, xf_c, yf_c)
            goTo_R_c = distance(xf, yf, xs_c, ys_c)
            t_travel = goTo_R + length_R + goTo_R_c
            end_R_and_R_c = max(t_free + goTo_R + length_R + goTo_R_c + length_R_c,
                        ts + length_R + goTo_R_c + length_R_c,
                        ts_c + length_R_c)

            if (i == 0):
                t_free_next = max(distance(xs_c, ys_c, 0, 0), ts_c) + distance(xs_c, ys_c, xf_c, yf_c)
            else:
                t_free_next = max(distance(xs_c, ys_c, xf_p, yf_p) + t_free, ts_c) + distance(xs_c, ys_c, xf_c, yf_c)

            if (max(t_free + goTo_R + length_R, ts + length_R) <= tf
                and end_R_and_R_c <= tf_c):
                success = True
                end_previous = end_R_and_R_c
                for j in range(i+1, len(orders[vID])):
                    r_c = rides[orders[vID][j]]
                    (xs_c, ys_c, xf_c, yf_c, ts_c, tf_c, ID_c) = r_c
                    r_p = rides[orders[vID][j-1]]
                    (xs_p, ys_p, xf_p, yf_p, ts_p, tf_p, ID_p) = r_p

                    goTo_R_c = distance(xf_p, yf_p, xs_c, ys_c)
                    length_R_c = distance(xs_c, ys_c, xf_c, yf_c)
                    end = max(end_previous + goTo_R_c + length_R_c, ts_c + length_R_c)
                    if (end > tf_c):
                        success = False
                        break
                    end_previous = end 

                if success:
                    return vID, i

            t_free = t_free_next
 
    # there is no holes that can fit r
    return -1, -1



def insertOrder(orders, vID, orderID, rideID):
    orders[vID].insert(orderID, rideID)
    

def solve(constraints, rides):
    """
        Generate the solution of the problem, that is a list in which each
        element is a list of the indexes of the rides assigned to the matching
        vehicule, and return it.
    """

    print("Sorting list of rides by priority...")

    # initialisation
    [rows, columns, vehiculesNb, ridesNb, bonus, stepsNb] = constraints
    sorted_rides = sortDeadline(rides)
    vInfo = initVehiculesInfo(vehiculesNb)
    orders = initOrders(vehiculesNb)

    print("    -> done.")
    print("Attributing a car to each ride if possible...")

    # find the most appropriate vehicule for each ride
    for ride in sorted_rides:
        vehiculeID = choice(vInfo, ride, stepsNb)
        if vehiculeID != -1 :
            setOrdre(orders, vehiculeID, ride[6])
            update(vInfo, vehiculeID, ride)

    print("    -> done.")
    print("Trying to insert unsassigned rides...")

    # get the list of unassigned rides sorted by descending length
    un_rides_ID = getUnassignedRidesID(orders, len(rides))
    unassigned = []
    for i in un_rides_ID:
        unassigned.append(rides[i])
    sortedUnassigned = sorted(unassigned, key=lambda r: distance(r[0], r[1], r[2], r[3]), reverse=True)

    # try to fit the unassigned rides into the holes
    for r in sortedUnassigned:
       vID, orderID = searchHole(orders, rides, r)
       if (vID != -1):
           insertOrder(orders, vID, orderID, r[6])

    print("    -> done.\n")

    return orders

